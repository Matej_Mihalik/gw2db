<?php include_once('header.php'); ?>

<?php
	include_once('functions.php');
	$conn = db_connect();

	$default_name = isset($_POST['name']) ? $_POST['name'] : '';
	$default_type = isset($_POST['type']) ? $_POST['type'] : '';
	$result = db_query($conn, 'SELECT DISTINCT(type) FROM item_details ORDER BY type');
?>

<form action="#" method="post" class="inline_form">
	<input type="text" id="FilterName" name="name" value="<?php echo "{$default_name}"; ?>" placeholder="Name">
	<select id="FilterType" name="type">
		<option value="%">All</option>
		<?php
			while($type = pg_fetch_object($result)) {
				echo "<option value=\"{$type->type}\"".($type->type===$default_type ? ' selected="selected"' : '').">{$type->type}</option>";
			}
		?>
	</select>
	<input type="submit" id="FilterSubmit" value="Search" />
</form>
<form action="index.php" method="post" class="inline_form">
	<input type="submit" id="FilterHome" value="Main page" />
</form>

<?php
	if(!empty($_POST)) {
		$result = db_query_params($conn, 'SELECT name, type, image_link FROM item_details WHERE name ILIKE $1 AND type LIKE $2', array('%'.$_POST['name'].'%', $_POST['type']));
		$num_rows = pg_num_rows($result);
		echo "<p>Found {$num_rows} items</p>";
		while($row = pg_fetch_object($result)) {
			echo "<div class='entry'>
				<div class='entry-name'>{$row->name}</div><div class='entry-type'>{$row->type}</div><div class='entry-image-small' style='background-image: url({$row->image_link})'></div><div class='entry-image-large' style='background-image: url({$row->image_link})'></div>
			</div>";
		}
	}

	db_close($conn);
?>

<?php include_once('footer.php'); ?>
