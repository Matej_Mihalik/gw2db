<?php include_once('header.php'); ?>

<?php
	include_once('functions.php');
	$conn = db_connect();

	db_query($conn, 'CREATE TABLE IF NOT EXISTS item_details (
		id bigserial PRIMARY KEY NOT NULL,
		item_id bigint UNIQUE NOT NULL,
		name varchar(255) NOT NULL,
		type varchar(32) NOT NULL,
		image_link varchar(255) NOT NULL,
		json_full text NOT NULL
	);');

	$result = db_query($conn, 'SELECT COUNT(*) FROM item_details');
	while($row = pg_fetch_object($result)) {
		echo "<p>Current items in DB: {$row->count}</p>";
	}

	db_close($conn);
?>

<form action="read.php" method="post">
	<input type="submit" value="Browse Database" />
</form>
<form action="update.php" method="post">
	<input type="submit" value="Update Database" />
</form>
<form action="write.php" method="post">
	<input type="submit" value="Fill Database" />
</form>

<?php include_once('footer.php'); ?>
