****************************************

     Guild Wars 2 Database (GW2DB)

****************************************

This is a simple PHP / PostgreSQL based application whose main function is to provide easier access to in-game items icons for use in other projects.



----------------------------------------

                 About

----------------------------------------

The application provides three main function, accessible from the main screen:

	- FILL DATABASE - using the Guild Wars 2 API (http://wiki.guildwars2.com/wiki/API:Main) 'items' and 'item_details' enpoints (http://wiki.guildwars2.com/wiki/API:1/items and http://wiki.guildwars2.com/wiki/API:1/item_details respectively), it clears and then fills the database specified in the settings.

	- UPDATE DATABASE - similar to previous function, but it doesn't clear and fill the database from scratch, rather, it updates the database with data only on those items that are not yet contained in the database.

	- BROWSE DATABASE - allows the user to browse the items in the database. There are filters for item 'name' and 'type', and for every result found based on the values of those filters it displays a row with item name, type, small item icon (32 x 32 px) and large item icon (64 x 64 px). The icons are displayed using the Guild Wars 2 API (http://wiki.guildwars2.com/wiki/API:Main) 'render' service (http://wiki.guildwars2.com/wiki/API:Render_service).



----------------------------------------

              Requirements

----------------------------------------

The application requires a PHP5 environment and an access to a PostgreSQL database.



----------------------------------------

                Settings

----------------------------------------

The 'settings.xml' file allows the users to configure the basic application options.

Option name      | Description
---------------- | ---------------------------------------------------------------------------------------
name             | Name of the PostgreSQL with which the application will work
username         | Username with which to connect to the database
password         | Password with which to connect to the database
items_url        | URL of the Guild Wars 2 API 'items' endpoint
item_details_url | URL of the Guild Wars 2 API 'item_details' endpoint
render_url       | URL of the Guild Wars 2 API 'render' service
image_format     | Format of the requested item icons. Available values are 'png' and 'jpg'
request_limit    | Max number of request made for one Guild Wars 2 API URL. At least one request is always made



----------------------------------------

           Database structure

----------------------------------------

Application stores all data in a single table 'item_details'. Structure of the table is as follows:

Column     | Type         | Description
---------- | ------------ | ---------------------------------------------------------------------------------------
id         | bigserial    | Internal row id
item_id    | bigint       | Item id as returned by Guild Wars 2 API
name       | varchar(255) | Name of the item
type       | varchar(32)  | Item type
image_link | varchar(255) | URL of the specific items icon using the Guild Wars 2 API 'render' service
json_full  | text         | Full json object returned by the Guild Wars 2 API 'item_details' endpoint



----------------------------------------

             Database dump

----------------------------------------

Database dump (from 2014-07-10) can be found in the 'db_dump' folder.

It was created using the following command:
	pg_dump -O -U {username} {database} > {filename}.sql

And can be imported using:
	psql -U {username} {database} < {filename}.sql



----------------------------------------

                License

----------------------------------------

Guild Wars 2 Database (GW2DB)
Copyright (C) 2014 Matej Mihalik <matej.mihalik.sk@gmail.com>
Licensed under the GNU Affero General Public License
See <http://www.gnu.org/licenses/agpl-3.0.html> or LICENSE.txt for details

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.



----------------------------------------

          Third party licenses

----------------------------------------

PT Serif
Copyright (C) 2009 ParaType Ltd.
with Reserved Names 'PT Sans' and 'ParaType'
Licensed under the Paratype PT Sans Free Font License
See <http://www.fontsquirrel.com/license/pt-serif> or LICENSE_FONT.txt for details



----------------------------------------

                Contact

----------------------------------------

Author: <matej.mihalik.sk@gmail.com>
Code repository: <https://bitbucket.org/Matej_Mihalik/gw2db>
