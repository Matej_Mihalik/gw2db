<?php include_once('header.php'); ?>

<?php
	include_once('functions.php');
	$conn = db_connect();
	db_prepare($conn, 'insert_item', 'INSERT INTO item_details (item_id, name, type, image_link, json_full) VALUES ($1, $2, $3, $4, $5)');
	db_prepare($conn, 'exists_item', 'SELECT EXISTS (SELECT 1 FROM item_details WHERE item_id=$1)');

	$settings = load_xml_file('settings.xml')->gw_api;
	$request_limit = $settings->request_limit;
	$items_json = read_remote_file($settings->items_url, $request_limit);
	$items_data = decode_json($items_json);
	$items_count = 0;
	$item_exists = 'f';

	foreach($items_data->items as $item_id) {
		set_time_limit(30);

		$result = db_execute($conn, 'exists_item', array($item_id));
		while($row = pg_fetch_object($result)) {
			$item_exists = $row->exists;
		}
		if($item_exists==='t') {
			continue;
		}

		$items_count++;
		$item_json = read_remote_file(str_replace('{item_id}', $item_id, $settings->item_details_url), $request_limit);
		$item_data = decode_json($item_json);

		db_execute($conn, 'insert_item', array($item_id, $item_data->name, $item_data->type, str_replace(array('{signature}', '{file_id}', '{format}'), array($item_data->icon_file_signature, $item_data->icon_file_id, $settings->image_format), $settings->render_url), $item_json));
	}
	echo "<p>Processed {$items_count} new items</p>";

	db_close($conn);
?>

<form action="index.php" method="post">
	<input type="submit" value="Main page" />
</form>

<?php include_once('footer.php'); ?>
