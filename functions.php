<?php

function db_connect() {
	$settings = load_xml_file('settings.xml')->local_db;

	$result = @pg_connect("dbname={$settings->name} user={$settings->username} password={$settings->password}");
	if($result===false) {
		exit('<p>Failed to open DB connection!</p>');
	}
	return $result;
}

function db_prepare($conn, $name, $query) {
	$result = @pg_prepare($conn, $name, $query);
	if($result===false) {
		exit('<p>Failed to prepare query: \''.pg_last_error($conn).'\'</p>');
	}
	return $result;
}

function db_execute($conn, $name, $params) {
	$result = @pg_execute($conn, $name, $params);
	if($result===false) {
		exit('<p>Failed to execute query: \''.pg_last_error($conn).'\'</p>');
	}
	return $result;
}

function db_query($conn, $query) {
	$result = @pg_query($conn, $query);
	if($result===false) {
		exit('<p>Query failed: \''.pg_last_error($conn).'\'</p>');
	}
	return $result;
}

function db_query_params($conn, $query, $params) {
	$result = @pg_query_params($conn, $query, $params);
	if($result===false) {
		exit('<p>Param query failed: \''.pg_last_error($conn).'\'</p>');
	}
	return $result;
}

function db_close($conn) {
	$result = @pg_close($conn);
	if($result===false) {
		exit('<p>Failed to close DB connection: \''.pg_last_error($conn).'\'</p>');
	}
	return $result;
}

function load_xml_file($file) {
	libxml_clear_errors();
	libxml_use_internal_errors(true);
	$xml = simplexml_load_file($file);

	if($xml===false) {
		echo "<p>Failed to open file: '{$file}'</p>";
		foreach(libxml_get_errors() as $error) {
			echo "<p>--> {$error->message}</p>";
		}
		exit();
	}
	return $xml;
}

function read_remote_file($url, $limit) {
	$count = 0;
	$file = @file_get_contents($url);

	while($file===false && ++$count<$limit) {
		$file = @file_get_contents($url);
	}

	if($file===false) {
		exit("<p>Failed to read remote URL: '{$url}'</p>");
	}
	return $file;
}

function decode_json($json) {
	$object = json_decode($json);
	if($object===null) {
		echo '<p>Failed to decode JSON: \''.json_last_error_msg().'\'</p>';
		exit("<p>{$json}</p>");
	}
	return $object;
}

?>
